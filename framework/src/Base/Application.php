<?php namespace Teil\Base;

use Teil\Container\Container;
use Teil\System AS TeilSystem;

/**
* Basic Teil
*/
class Application extends Container {
	

	function __construct() {
		// Get global providers
		$this->providers = $GLOBALS['teilProviders'];
	}


	/**
	 * Get the service provider repository instance.
	 *
	 */
	public function getProviderRepository()
	{
		return new TeilSystem\ProviderRepository($this->providers);
	}

	
	/**
	 * Register a service provider with the application.
	 *
	 */
	public function register($provider, $options = array())
	{
		if (is_string($provider))
		{
			$provider = $this->resolveProviderClass($provider);
		}

		$provider->register();
	}


	/**
	 * Resolve a service provider instance from the class name.
	 *
	 */
	protected function resolveProviderClass($provider)
	{
		return new $provider($this);
	}
	
}