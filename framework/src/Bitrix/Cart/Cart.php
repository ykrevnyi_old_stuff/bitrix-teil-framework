<?php namespace Teil\Bitrix\Cart;


/**
 * User cart
 */
class Cart {

	/**
	 * Cart actions
	 */
	private $allAction;
	private $insertAction;
	private $updateAction;
	private $deleteAction;


	/**
	 * Params to be passed to the action
	 */
	private $params = array();


	function __construct(
			$allAction, 
			CartActionInterface $insertAction, 
			CartActionInterface $updateAction, 
			CartActionInterface $deleteAction)
	{
		$this->allAction = $allAction;
		$this->insertAction = $insertAction;
		$this->updateAction = $updateAction;
		$this->deleteAction = $deleteAction;
	}


	/**
	 * Execute cart action (depends on argument passed)
	 *
	 * @return mixed
	 */
	public function executeAction($method = NULL, $params = array())
	{
		if (is_null($method))
		{
			throw new \Exception("Cart::action() You shoul pass \$method!");
		}

		// Merge params
		$params = $this->mergeParams($params);

		// Execute action
		switch ($method)
		{
			case 'all':
				return $this->allAction->make($filter, $params);
			break;

			case 'insert':
				return $this->insertAction->make($params);
			break;

			case 'update':
				return $this->updateAction->make($params);
			break;

			case 'delete':
				return $this->deleteAction->make($params);
			break;
		}
	}


	/**
	 * Add setting to the action object
	 *
	 * @return void
	 */
	public function addParams($params, $override = false)
	{
		// If override set to be true ---> $this->params will be recreated
		if ($override)
		{
			$this->params = $params;
		}
		else
		{
			$this->params = array_merge($this->params, $params);
		}
	}


	/**
	 * Simpply merge default and user params
	 *
	 * @return void
	 */
	public function mergeParams($params = array())
	{
		return array_merge($this->params, $params);
	}


	/**
	 * Simply return current params
	 *
	 * @return array
	 */
	public function getParams()
	{
		return $this->params;
	}


	/**
	 * Magicly catch `action` methods, and call $this->executeAction()
	 *
	 * @return mixed
	 */
	public function __call($method, $params)
	{
		// Call method if it exists
		if (method_exists($this, $method))
		{
			return call_user_method_array($method, $this, $params);
		}

		// Call $this->executeAction()
		elseif (preg_match("/^(all|insert|update|delete)$/i", $method))
		{
			return $this->executeAction($method, $params);
		}

		else
		{
			throw new \Exception("Cart:: Method `" . $method . "` is not exist!");
		}
	}


}