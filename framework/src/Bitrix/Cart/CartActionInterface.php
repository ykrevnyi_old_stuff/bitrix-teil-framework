<?php namespace Teil\Bitrix\Cart;

interface CartActionInterface {
	
	/**
	 * Basic callable.
	 *
	 * @return mixed
	 */
	public function make($data = array());
	
}