<?php namespace Teil\Bitrix\Cart;

use Teil\System\Facades AS F;

/**
* Class stands for adding product to the basket
*/
class ActionAll
{
	/**
	 * Default params for cart
	 */
	private $defaultParams = array(
		array("NAME" => "ASC", "ID" => "ASC"),
		array(),
		false,
		false,
		array()
	);


	function __construct() {}

	
	/**
	 * Init
	 *
	 * @return array
	 */
	public function make($filter = NULL, $params = array())
	{
		// Set filter
		if ( ! is_null($filter))
		{
			$this->defaultParams[1] = $filter;
		}
		else
		{
			$this->defaultParams[1] = array(
				"FUSER_ID" => \CSaleBasket::GetBasketUserID(),
				"LID" => SITE_ID,
				"ORDER_ID" => "NULL"
			);
		}

		// Merge user and default arrays
		$params = array_merge($params, $this->defaultParams);

		$items = $this->all($params);
		$total_num_of_products = $this->getTotalNumProducts($items);

		return array(
			'items' => $items,
			'total' => $total_num_of_products,
			'total_products' => F\DW::make($total_num_of_products),
			'prices' => $this->getTotalPrice($items)
		);
	}


	/**
	 * Get all cart items
	 *
	 * @return array
	 */
	public function all($params = array())
	{
        $resultCartProducts = array();

        $demo_params = array(
        	array(
			"NAME" => "ASC",
			"ID" => "ASC"
			),
			array(
			"FUSER_ID" => \CSaleBasket::GetBasketUserID(),
			"LID" => SITE_ID,
			"ORDER_ID" => "NULL"
			),
			false,
			false,
			array()
    	);

        // Get cart items
        // Here we shouldn't pass an ARRAY of params.
        // That's why params look like this
        $dbCart = \CSaleBasket::GetList(
        	$params[0], 
        	$params[1], 
        	$params[2], 
        	$params[3], 
        	$params[4]
    	);

		while ($cartProduct = $dbCart->Fetch())
		{
			// Get product properties in cart
			$cartProduct["PROPS"] = $this->getProperties($cartProduct);

			$resultCartProducts[] = $cartProduct;
		}

        return $resultCartProducts;
	}


	/**
	 * Get properties of cart product
	 *
	 * @return array
	 */
	public function getProperties($cartProduct = NULL)
	{
		if (is_null($cartProduct))
		{
			throw new \Exception("Cart::getProperties() You shoul pass cart product!");
		}

		$properties = array();

		// Get all cart product properties
		$dbCartProperties = \CSaleBasket::GetPropsList(
			array("SORT" => "ASC", "ID" => "ASC"),
			array(
				"BASKET_ID" => $cartProduct["ID"], 
				"!CODE" => array(
					"CATALOG.XML_ID", 
					"PRODUCT.XML_ID"
				)
			)
		);

		while ($propetry = $dbCartProperties->GetNext())
		{
			$properties[] = $propetry;
		}

		return $properties;
	}


	/**
	 * Get total price
	 *
	 * @return void
	 */
	private function getTotalPrice($items = array())
	{
		$total_price = '';

		foreach ($items as $item)
		{
			$total_price += $item['PRICE'] * $item['QUANTITY'];
		}

		return array(
			'total_price' => CurrencyFormat($total_price, 'RUB'),
			'_total_price' => $total_price
		);
	}


	/**
	 * Get total number of products
	 *
	 * @return int
	 */
	private function getTotalNumProducts($items = array())
	{
		$total_number_of_products = 0;

		foreach ($items as $item)
		{
			$total_number_of_products += $item['QUANTITY'];
		}

		return $total_number_of_products;
	}

}