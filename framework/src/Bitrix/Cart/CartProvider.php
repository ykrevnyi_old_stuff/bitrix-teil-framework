<?php namespace Teil\Bitrix\Cart;

use Teil\System\ServiceProvider;


class CartProvider extends ServiceProvider {


	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		$this->registerCart();
	}


	/**
	 * Register the cart builder instance.
	 *
	 * @return void
	 */
	protected function registerCart()
	{
		$cart = new Cart(
			new ActionAll,
			new ActionInsert,
			new ActionUpdate,
			new ActionDelete
		);

		$this->app->instance('Cart', $cart);
	}


	/**
	 * Get the services provided by this provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return array('Cart');
	}


}