<?php namespace Teil\Bitrix\Cart;

use Teil\System\Facades AS F;

/**
* Class stands for adding product to the basket
*/
class ActionInsert implements CartActionInterface
{
	

	/**
	 * Simply adds product to the cart with properties
	 *
	 * @return mixed
	 */
	public function make($args = array())
	{
		$data = $args[0];

		if(empty($data['product_id']))
        {
            throw new \Exception("ActionInsert::make() No `product_id` specified!");
        }

        // Check if product exists AND get actual product ID
        $product_id = $this->productExists($data['product_id']);
        
        // Add to cart
        $status = $this->executeAssertion(
        	$product_id, 
        	$data['quantity'], 
    		$data['props']
    	);

        // Fail
        if( ! $status)
        {
            return array(
            	'status' => 'fail',
            	'message' => 'Basket add error'
        	);
        }

        // Success
        else
        {
        	return array(
        		'status' => 'success',
        		'cart' => F\Cart::all()
    		);
        }
	}


	/**
	 * Insert action
	 *
	 * @return string
	 */
	private function executeAssertion($product_id, $quantity, $props)
	{
		// If we dont specify how many product we want to add
		// There would be 1 product, by default
		if (empty($quantity) OR ! $quantity)
		{
			$quantity = 1;
		}

		if (empty($props) OR ! $props)
		{
			$props = array();
		}

		// Add this shit to basket ;)
		return Add2BasketByProductID(
        	$product_id, 
        	$quantity, 
        	NULL, 
    		$props
		);
	}


	/**
	 * Check if there is some product with id of ID
	 *
	 * @return int
	 */
	private function productExists($product_id)
	{
		$product = \CCatalogProduct::GetByIDEx($product_id);
                
        if ( ! $product)
        {
            throw new \Exception(
            	"ActionInsert::make() No product with id of `product_id` found!"
        	);
        }
        
        // Get actual product id
        return $product['PRODUCT']['ID'];
	}

}