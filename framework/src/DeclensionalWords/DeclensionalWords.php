<?php namespace Teil\DeclensionalWords;


/**
 * Words in different cases
 *
 */
class DeclensionalWords 
{

	/**
	 * Default string (word) to be case-converted
	 */
	private $word = "товар";


	function __construct() {}


	/**
	 * Change case-standing word
	 *
	 * @return void
	 */
	public function setWord($value) { $this->word = $value;	}
	public function getWord() { return $this->word; }


	/**
	 * Convert name into other case
	 *
	 * @return string
	 */
	public function make($total_num)
	{
		return $this->word . $this->getNameEnding($total_num);
	}


	/**
	 * Get name ending depends on last digit (ex. 108 ---> last digit is 8)
	 *
	 * @return string
	 */
	public function getNameEnding($total_num = NULL)
	{
		$last_digit = $this->getLastDigit($total_num);

		if (is_null($total_num))
		{
			throw new \Exception("DW::getNameEnding() You shoud pass argument!");
		}

		if ($last_digit == 0)
		{
			return "ов";
		}
		
		elseif ($total_num >= 10 AND $total_num <= 20)
		{
			return "ов";
		}
		
		elseif ($last_digit == 1)
		{
			return "";
		}
		
		elseif ($last_digit >= 2 AND $last_digit <= 4)
		{
			return "а";
		}
		
		elseif ($last_digit >= 5 AND $last_digit <= 9)
		{
			return "ов";
		}
	}


	/**
	 * Simply get the last digit of some number
	 *
	 * @return Number
	 */
	public function getLastDigit($num)
	{
		return preg_replace("/.*(\d)$/", "$1", $num);
	}

}