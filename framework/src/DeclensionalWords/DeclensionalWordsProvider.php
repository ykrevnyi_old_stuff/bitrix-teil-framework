<?php namespace Teil\DeclensionalWords;

use Teil\System\ServiceProvider;


class DeclensionalWordsProvider extends ServiceProvider {


	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		$this->registerDeclensionalWords();
	}


	/**
	 * Register the HTML builder instance.
	 *
	 * @return void
	 */
	protected function registerDeclensionalWords()
	{
		$this->app->instance('DW', new DeclensionalWords);
	}


	/**
	 * Get the services provided by this provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return array('DW');
	}


}