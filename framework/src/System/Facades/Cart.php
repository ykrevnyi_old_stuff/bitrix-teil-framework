<?php namespace Teil\System\Facades;


/**
 * See Html\HtmlBuilder
 */
class Cart extends Facade {


	/**
	 * Get the registered name of the component.
	 *
	 * @return string
	 */
	protected static function getFacadeAccessor() { return 'Cart'; }


}