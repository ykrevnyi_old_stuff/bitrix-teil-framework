<?php namespace Teil\System\Facades;


/**
 * See DeclensionalWords\DeclensionalWords
 */
class DW extends Facade {


	/**
	 * Get the registered name of the component.
	 *
	 * @return string
	 */
	protected static function getFacadeAccessor() { return 'DW'; }


}