<?php namespace Teil\System\Facades;


/**
 * See Html\HtmlBuilder
 */
class MenuBuilder extends Facade {


	/**
	 * Get the registered name of the component.
	 *
	 * @return string
	 */
	protected static function getFacadeAccessor() { return 'MenuBuilder'; }


}