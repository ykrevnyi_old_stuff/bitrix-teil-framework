<?php namespace Teil\MenuBuilder;


/**
 * Generate menu recursively
 *
 */
class MenuBuilder 
{

	/**
	 * Contains built menu result
	 */
	private $result = "";


	/**
	 * Basic menu item template
	 */
	private $parent_menu_item_template = "<a class='{{class}}' href='{{href}}'><div style='background: url({{image}});'></div></a>";
	private $children_list_wrapper = "<ul class='menu_tree'>{{content}}</ul>";
	private $submenu_item_template = "<li><a href='{{href}}' class='menu_style1'>{{name}}</a></li>";

	/**
	 * Construct :)
	 *
	 * @return void
	 */
	function __construct() {}


	/**
	 * Get/Set result template
	 *
	 * @return string
	 */
	public function getResult() { return $this->result; }
	public function setResult($value) { $this->result = $value; }


	/**
	 * Get/Set parent menu item template
	 *
	 * @return string
	 */
	public function getParentMenuItem() { return $this->parent_menu_item_template; }
	public function setParentMenuItem($value) { $this->parent_menu_item_template = $value; }


	/**
	 * Get/Set submenu item template
	 *
	 * @return string
	 */
	public function getSubMenuItem() { return $this->submenu_item_template; }
	public function setSubMenuItem($value) { $this->submenu_item_template = $value; }


	/**
	 * Get/Set default children template wrapper
	 *
	 * @return string
	 */
	public function getChildrenTemplateWrapper() { return $this->children_list_wrapper; }
	public function setChildrenTemplateWrapper($value) { $this->children_list_wrapper = $value; }


	/**
	 * Format tree
	 *
	 * @return string
	 */
	public function tree($nodes = NULL)
	{
		if (is_null($nodes))
		{
			throw new \Exception("MenuBuilder::tree() Should pass an $nodes array!");
		}

		foreach ($nodes as $node)
		{
			$this->dump($node);
		}

		return str_replace(
			'{{content}}',
			$this->getResult(),
			$this->children_list_wrapper
		);
	}


	/**
	 * Dump menu child
	 *
	 * @return void
	 */
	public function dump($node) {
		// Count all the children of current node
		$children = isset($node['CHILDS']) ? count($node['CHILDS']) : 0;

		// Fetch and store PARENT menu item template
		$template = str_replace("</li>", "", $this->parent_menu_item_template);

		$this->result .= $this->fetchTemplate($template, $node);

		// If current item doesnt have children --> stop execution
		if ( ! $children)
		{
			$this->result .= "</li>";
			return false;
		}

		$this->result .= "<ul class='category-details-list pimi-girl'>";

		foreach ($node['CHILDS'] as $child)
		{
			$this->result .= $this->fetchTemplate($this->submenu_item_template, $child);

			if (isset($child['CHILDS']) AND count($child['CHILDS']))
			{
				$this->dump($child, $child["ID"]);
			}
		}

		$this->result .= "</ul>";
		$this->result .= "</li>";
	}


	/**
	 * Get menu wrapper for chuildren list
	 *
	 * @return string
	 */
	public function getChildrenWrapperPart($part = NULL)
	{
		// We should specify a $part to be returned (ex. <ul> OR </ul>)
		if (is_null($part))
		{
			throw new \Exception("MenuBuilder::getChildrenWrapperPart() Please, specify children wrapper part to be returned!");
		}

		$wrapper = explode('{{content}}', $this->children_list_wrapper);

		return ($part == 'start') ? $wrapper[0] : $wrapper[1];
	}


	/**
	 * Replace {{shortcuts_like_this}} with text
	 *
	 * @return string
	 */
	public function fetchTemplate($template, $node)
	{
		if ( ! $template OR ! $node)
		{
			throw new \Exception("MenuBuilder::fetchTemplate() Arguments should be specified!");
		}

		$r = str_replace('{{class}}', 'pimi_girl', $template);
		$r = str_replace('{{href}}', $node['HREF'], $r);
		$r = str_replace('{{image}}', $node['IMAGE'], $r);
		$r = str_replace('{{name}}', $node['NAME'], $r);

		return $r;
	}

}