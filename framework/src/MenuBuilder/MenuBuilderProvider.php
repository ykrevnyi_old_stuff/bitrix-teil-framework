<?php namespace Teil\MenuBuilder;

use Teil\System\ServiceProvider;


class MenuBuilderProvider extends ServiceProvider {


	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		$this->registerMenuBuilder();
	}


	/**
	 * Register the HTML builder instance.
	 *
	 * @return void
	 */
	protected function registerMenuBuilder()
	{
		$this->app->instance('MenuBuilder', new MenuBuilder);
	}


	/**
	 * Get the services provided by this provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return array('MenuBuilder');
	}


}