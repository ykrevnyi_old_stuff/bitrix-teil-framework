<?php namespace Teil;

use Teil\System\Facades AS Facades;


/**
 * Import basic providers, such as:
 * - html
 * - form
 * - billing
 * - etc
 */
require_once("providers.php");


/**
 * Create basic application instance.
 * It contains all injected instances.
 *
 */
$app = new Base\Application();


/**
 * Self-injected application instance
 *
 */
$app->instance('app', $app);


/**
 * Facades setup.
 *
 * Clear all `before resolved` instances
 * Set application to build on.
 *
 */
Facades\Facade::clearResolvedInstances();
Facades\Facade::setFacadeApplication($app);


/**
 * Register providers.
 * 
 * WARNING: see /vendor/teil/framework/providers.php
 *
 */
$app->getProviderRepository()->load($app);