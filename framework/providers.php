<?php 

$GLOBALS['teilProviders'] = array(
	'providers' => array(
		"Teil\\Html\\HtmlServiceProvider",
		"Teil\\MenuBuilder\\MenuBuilderProvider",
		"Teil\\DeclensionalWords\\DeclensionalWordsProvider",
		"Teil\\Bitrix\\Cart\\CartProvider"
	)
);